<?php

return [

    # cockpit session name
    'app.name' => 'Rekord Gaming',

    # cockpit session name
    'session.name' => 'rekordgaming',

    # app custom security key
    'sec-key' => 'ADA#$@SDFSasfsdsd234',

    # site url (optional) - helpful if you're behind a reverse proxy

    # define the languages you want to manage
    'languages' => [
        'pl' => 'Polish',
        'en' => 'English'
    ],

    # define additional groups
    'groups' => [
        'author' => [
            '$admin' => false,
            '$vars' => [
                'finder.path' => '/storage/upload'
            ],
            'cockpit' => [
                'backend' => true,
                'finder' => true
            ],
            'collections' => [
                'manage' => true
            ]
        ]
    ],

    # use mongodb as main data storage
    'database' => [   
        'server' => 'mongodb://localhost:27017',
        'options' => [
            'db' => 'cockpitdb'
        ]
    ],

    # use smtp to send emails
    'mailer' => [
        'from'       => 'info@mydomain.tld',
        'transport'  => 'smtp'
        'host'       => 'smtp.myhost.tld',
        'user'       => 'username'
        'password'   => 'xxpasswordxx',
        'port'       => 25,
        'auth'       => true,
        'encryption' => '' # '', 'ssl' or 'tls'
    ]
];
# Polish Cockpit in Docker

## What you need?

1. Docker
2. Free space on the computer :P

### Change config in config.php

## How to run?

Just run:

```bash
docker build -t YOUR_NAME
```

then:

```
docker run --name NAME -p 8080:80 -rm -d YOUR_NAME
```

### Open [this](http://localhost:8080/install) in browser

### Login

### Then change language to polish in account settings

### And thats It!
